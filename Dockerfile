FROM python:alpine

RUN apk add gcc zlib-dev libjpeg-turbo-dev musl-dev

COPY . /opt/

WORKDIR /opt

RUN pip install -r requirements.txt

CMD ["python", "-u", "runbot.py"]
