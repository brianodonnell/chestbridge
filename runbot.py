#!/usr/bin/env python3

import logging, sys, time, yaml
from lib.bot import Bot

"""
A silly bot for [matrix]
"""


def __main__():
    logging.basicConfig(
        format="%(asctime)s %(levelname)s\t%(message)s", level=logging.INFO
    )
    logging.info("Loading config from config.yml")
    with open("config.yml", "r") as file:
        conf = yaml.load(file, Loader=yaml.Loader)
        for (key, value) in conf.items():
            logging.debug(f"    {key}: {value}")
    bot = Bot(
        display_name=conf["display_name"],
        host=conf["host"],
        openweather_api_key=conf["openweather_api_key"],
        cache_db="db/cache.db",
    )
    bot.login(conf["username"], conf["password"])
    bot.start_listening()
    while bot.client.should_listen:
        # check every n seconds for empty rooms
        time.sleep(900)
        try:
            for room in bot.rooms.values():
                bot.leave_if_empty(room)
        except RuntimeError:
            pass
    bot.stop_listening()


__main__()
