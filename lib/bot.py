import logging, re, time
from matrixbot import MatrixBot
from lib.actionpack import ActionPack

logger = logging.getLogger(__name__)


class Bot(MatrixBot):
    def __init__(
        self,
        host="matrix.org",
        display_name="MatrixBot",
        token=None,
        user_id=None,
        cache_db=None,
        openweather_api_key=None,
        enable_previews=False,
    ):
        super().__init__(
            host=host,
            display_name=display_name,
            token=token,
            user_id=user_id,
            cache_db=cache_db,
        )
        self.openweather_api_key = openweather_api_key
        self.enable_previews = enable_previews
        self.ap = ActionPack(cache=self.cache)

    def process_event(self, room, event):
        content = event["content"]
        sender = event["sender"]
        # don't talk to yourself
        if sender == self.user.user_id:
            return
        logger.debug(f"Incoming event data: {content}")
        if content["msgtype"] == "m.text":
            m = re.match("\!cb (.*)", content["body"])
            if m:
                logger.debug(f"{content['body']} matches. Parsing.")
                self.parse_command(room, sender, m.group(1))
                return
            elif self.enable_previews:
                m = re.search("(https?:\/\/[^ ]+)", content["body"])
                if m:
                    url = re.sub("\.$", "", m.group(1))
                    logger.info(
                        f"{sender} posted {url} to '{room.display_name}' ({room.room_id}). Generating a preview."
                    )
                    self.ap.post_preview(self.client, room, url)

    def best_match(self, string, wordlist):
        """
        return the best match for a partial word
        """
        l_string = string.lower()
        if l_string in wordlist:
            logger.debug(f"'{string}' found in wordlist.")
            return l_string
        else:
            for word in sorted(wordlist):
                if re.match(l_string, word):
                    logger.debug(f"'{string}' not in wordlist. Matching to '{word}'.")
                    return word
        logger.debug(f"No match found for '{string}'.")

    def parse_command(self, room, sender, command):
        """
        figure out what the user wants
        """
        words = command.split(" ")
        wordcount = len(words)
        if wordcount > 0:
            m = re.match("([a-zA-z0-9_]+)", words[0])
            if m:
                user = self.client.get_user(sender)
                logger.info(
                    f"Command '{command}' sent by '{user.get_display_name()}' ({user.user_id}) in room '{room.display_name}' ({room.room_id})."
                )
                action = self.best_match(words[0], self.ap.actions.keys())
                if action == "say" and wordcount > 1:
                    text = " ".join(words[1:])
                    self.ap.action_say(room, text)
                elif action == "help":
                    self.ap.help(room, user=user)
                elif action == "weather":
                    if wordcount >= 2:
                        zip = words[1]
                        if wordcount >= 3:
                            country = words[2]
                        else:
                            country = "us"
                        self.ap.action_weather(
                            room, self.openweather_api_key, zip, country, user=user
                        )
                elif action == "bitcoin":
                    if wordcount > 1:
                        code = words[1]
                    else:
                        code = "USD"
                    self.ap.action_bitcoin(room, code, user=user)
                elif action == "coinflip":
                    self.ap.action_coinflip(room, user=user)
                elif action == "qrandom":
                    if wordcount > 1:
                        digits = int(words[1])
                        if digits > 1024:
                            digits = 1024
                    else:
                        digits = 6
                    self.ap.action_qrandom(room, digits, user=user)
                else:
                    self.ap.action_say(room, "idkwtf", user=user)
