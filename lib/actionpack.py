import logging, markdown, quantumrandom, re, requests, time
from hashlib import md5
from io import BytesIO
from opengraph import OpenGraph
from PIL import Image

logger = logging.getLogger(__name__)


class ActionPack:
    """
    things what the bot can do
    """

    def __init__(self, cache=None):
        self.cache = cache
        self.actions = {
            "help": "",
            "bitcoin": "ISO 4217 currency code (default: USD)",
            "coinflip": "",
            "qrandom": "num_digits (default: 6, max: 1024)",
            "say": "your text here",
            "weather": "postal_code country_code (default: US)",
        }
        self.weather_icons = {
            "01": "🌞",
            "02": "🌤️",
            "03": "☁️",
            "04": "☁️",
            "09": "☔",
            "10": "🌧️",
            "11": "⛈️",
            "13": "❄️",
            "50": "🌈",
        },
        self.rng = quantumrandom.cached_generator()

    def mention_link(self, user):
        display_name = user.get_display_name()
        return f"[{display_name}](https://matrix.to/#/{user.user_id})"

    def greeting(self, room):
        """
        say this on entering a room
        """
        room.send_html(
            "Let's get straight to the biscuits: "
            + "type <strong>!cb help</strong> for a list of commands, and as always, "
            + "<strong>KILL HITLER!</strong>"
        )

    def help(self, room, user=None):
        """
        print the help text
        """
        helptext = (
            "Type **!cb command** *[arguments]*.\n"
            + "Valid commands are:\n\n"
            + "\n".join(
                [f"* **{x}** *[{self.actions[x]}]*" for x in self.actions.keys()]
            )
            + "\n\n"
            + "You can also type partial commands and I'll try to guess what you mean."
        )
        html = markdown.markdown(re.sub("\*\[]\*", "", helptext))
        self.action_say(room, html, user=user)

    def get_opengraph(self, url):
        """
        fetches opengraph data for a url
        """
        return OpenGraph(url=url)

    def create_thumbnail(self, image_data, max_width=400, max_height=280):
        """
        returns a thumbnail of an image
        """
        thumbnail = BytesIO()
        img = Image.open(BytesIO(image_data))
        # convert to RGB to save as JPEG
        rgb = img.convert("RGB")
        rgb.thumbnail((max_width, max_height), Image.ANTIALIAS)
        rgb.save(thumbnail, "JPEG", optimize=True, progressive=True, quality=50)
        return thumbnail.getvalue()

    def upload_from_url(self, client, url):
        """
        download the contents of a url and upload the result to matrix
        return an mxc url to the resource
        """
        r = requests.get(url)
        if r.ok:
            content_type = r.headers.get("content-type")
            if re.match("image\/", content_type):
                content = self.create_thumbnail(r.content)
            else:
                content = r.content
            # calculate checksum for caching purposes
            m = md5()
            m.update(content)
            mxc_url = self.cache.fetch(
                f"upload_{m.hexdigest()}",
                lambda: client.upload(content, content_type),
                timeout=(86400 * 3),
            )
            return mxc_url

    def post_image(self, client, room, url, user=None):
        """
        takes a url to an image, and posts that image to the room
        """
        mxc_url = self.upload_from_url(client, url)
        room.send_image(mxc_url, "preview.jpg")

    def post_preview(self, client, room, url):
        """
        post a linked title and thumbnail image for a link
        """
        logger.info(f"    Fetching preview data for {url}")
        r = requests.get(url)
        if r.ok:
            content_type = r.headers.get("content-type")
            if re.match("image\/", content_type):
                self.post_image(client, room, url)
            else:
                ogdata = self.get_opengraph(url)
                if ogdata is not None:
                    base_url = re.match("(https?:\/\/[^/]+)", url).group()
                    try:
                        site_name = ""
                        site_url = url
                        if hasattr(ogdata, "site_name"):
                            site_name = ogdata.site_name
                        else:
                            # if no site name specified, use the domain, less 'www.'
                            domain = re.match("https?:\/\/([^/]+)", base_url).group(1)
                            site_name = re.sub("^www\.", "", domain)
                        if hasattr(ogdata, "url"):
                            # if the ogdata specifies a url,
                            # use that instead of the original
                            site_url = ogdata.url
                        preview_title = f"[{site_name} - {ogdata.title}]({site_url})\n"
                        self.action_say(room, preview_title)
                        if ogdata.image is not None:
                            if not re.match("https?:\/\/", ogdata.image):
                                self.post_image(
                                    client, room, f"{base_url}{ogdata.image}"
                                )
                            else:
                                self.post_image(client, room, ogdata.image)
                    except AttributeError as e:
                        logger.error(f"    Failed to fetch preview data for {url}: {e}")
                        pass

    def fetch_json_api(self, url):
        r = requests.get(url)
        if r.ok:
            return r.json()

    def fetch_weather(self, api_key, zip, country="us"):
        """
        get the weather from openweathermap.org
        """
        logger.info("Fetching weather")
        return self.fetch_json_api(
            f"https://api.openweathermap.org/data/2.5/weather?APPID={api_key}&units=metric&zip={zip},{country}"
        )

    def fetch_bitcoin(self, code):
        logger.info("Fetching bitcoin rates")
        return self.fetch_json_api(
            f"https://api.coindesk.com/v1/bpi/currentprice/{code}.json"
        )

    def action_bitcoin(self, room, code="usd", user=None):
        data = self.cache.fetch(
            f"bitcoin_{code}", lambda: self.fetch_bitcoin(code.lower()), timeout=900
        )
        if data is not None:
            rate = data["bpi"][code.upper()]["rate_float"]
            description = data["bpi"][code.upper()]["description"]
            self.action_say(
                room,
                f"1 Bitcoin (BTC) = **{rate} {description} ({code.upper()})** "
                # + "\n"
                + "<sub>[powered by [CoinDesk](https://www.coindesk.com/price/bitcoin)]</sub>",
                user=user,
            )

    def action_coinflip(self, room, user=None):
        """
        flip a coin
        """
        if int(quantumrandom.randint(0, 2)) == 1:
            self.action_say(room, "👍", user=user)
        else:
            self.action_say(room, "👎", user=user)

    def action_qrandom(self, room, digits, user=None):
        """
        return a number from the ANU Quantum Random Numbers Server
        """
        result = "The gods of the quantum void say `"
        for d in range(0, digits):
            result += f"{int(quantumrandom.randint(0,10, generator=self.rng))}"
        result += '`'
        self.action_say(room, result, user=user)

    def action_say(self, room, text, user=None):
        """
        post text to the room
        """
        if user is None:
            html = markdown.markdown(text)
        else:
            html = markdown.markdown(f"{self.mention_link(user)} {text}")
        room.send_html(html)

    def action_weather(self, room, api_key, zip, country, user=None):
        """
        show the current weather
        """
        # make sure the zip looks at least marginally sane
        if re.match("[A-Za-z0-9-]+", zip):
            # cache the results for 15 minutes to prevent API abuse
            data = self.cache.fetch(
                f"weather_{zip}",
                lambda: self.fetch_weather(api_key, zip, country),
                timeout=900,
            )
            if data is not None:
                # harvest the tasty weather meats
                location = data["name"]
                icon = re.sub("[a-z]", "", data["weather"][0]["icon"])
                description = data["weather"][0]["description"]
                tempC = int(data["main"]["temp"])
                tempF = int(tempC * 9 / 5) + 32
                if icon in self.weather_icons.keys():
                    symbol = self.weather_icons[icon]
                else:
                    symbol = ""
                self.action_say(
                    room,
                    f"**{location}** is currently **{tempF} °F** ({tempC} °C) with **{description}** {symbol}",
                    user=user,
                )
            else:
                self.action_say(
                    room, "Sorry, I can't talk to the API right now.", user=user
                )
        else:
            self.action_say(room, "I don't think that's a real postal code.", user=user)
